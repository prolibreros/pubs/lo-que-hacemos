Esta expresión resultó poco precisa. La intención era decir que nadie tendría que pagar por el permiso para usar el sistema +++gnu+++. Pero la expresión no es del todo clara y a menudo se interpreta que las copias de +++gnu+++ deberían distribuirse siempre a un costo bajo o sin costo. Esta nunca fue la intención. Más adelante, el manifiesto menciona la posibilidad de que las empresas provean servicios de distribución para obtener ganancias. A partir de entonces, aprendí a distinguir cuidadosamente entre «*free*» (libre) en el sentido de libertad y «*free*» (gratis) en referencia al precio [en inglés, el término «free» puede significar libertad o gratuidad; N. del T.]. El *software* libre es aquel que ofrece a los usuarios la libertad de distribuirlo y modificarlo. Algunos pueden obtener copias sin pagar, mientras que otros pagan para obtenerlas, y si los fondos ayudan a mejorar el *software* es mucho mejor. Lo importante es que todos los que posean una copia tengan la libertad de colaborar con los demás al usar el programa.

+++gnu+++ se pronuncia en inglés de forma muy similar a «*new*», que significa «nuevo». [N. del T.].

La expresión «regalar» es otro indicio de que yo todavía no había separado claramente la cuestión de la gratuidad de la cuestión de la libertad. Ahora recomendamos no usar esta expresión al hablar acerca del *software* libre. Para una explicación más detallada, véase el artículo «[Palabras y frases confusas que vale la pena evitar](http://www.gnu.org/philosophy/words-to-avoid.es.html)».

Aquí también omití distinguir cuidadosamente entre los dos diferentes significados de «*free*» [que en inglés puede significar «gratis» o «libre»; N. del T.]. La afirmación tal como está escrita no es falsa, se pueden obtener copias gratuitas de *software* de +++gnu+++ ---de los amigos o a través de internet---, pero sugiere una idea errónea.

Ya existen varias compañías de este tipo.

Aunque es una organización sin ánimo de lucro más que una empresa, la Free Software Foundation (+++fsf+++) durante diez años ha obtenido la mayoría de los fondos a partir de su servicio de distribución. Puedes comprar artículos de la +++fsf+++ para apoyar su actividad.

Alrededor de 1991 un grupo de empresas de informática reunió fondos para apoyar el mantenimiento del compilador de C de +++gnu+++.

Creo que me equivoqué al decir que el *software* privativo era la base más común para ganar dinero en el campo del *software*. Parece ser que en realidad el modelo de negocio más común era y es el desarrollo de *software* a medida, que no ofrece la posibilidad de percibir una renta, por lo que la empresa tiene que seguir haciendo el trabajo real para seguir recibiendo ingresos. El negocio del *software* a medida podrá seguir existiendo, más o menos igual, en un mundo de *software* libre. Por lo tanto, ya no supongo que los programadores ganarían menos en un mundo de *software* libre.

En la década de los ochenta todavía no me había dado cuenta de lo confuso que era hablar de la «cuestión» de la «propiedad intelectual». Esa expresión es obviamente prejuiciosa, más sutil es el hecho de que agrupa leyes dispares que plantean cuestiones muy diferentes. Hoy en día insto a la gente a rechazar completamente el término «propiedad intelectual», para no inducir a otros a pensar que esas leyes forman un tema coherente. Para hablar con claridad, hay que referirse a las patentes, el *copyright* y las marcas registradas por separado. Véase el artículo «[¿Ha dicho "propiedad intelectual"? Es solo un espejismo seductor](http://www.gnu.org/philosophy/not-ipr.es.html)» para ver cómo esta expresión genera confusión y prejuicios.

Véase el [caso XYZ](https://es.wikipedia.org/wiki/Caso_XYZ) para más información sobre el contexto de esta sentencia. [N. del T.].

Posteriormente aprendimos a distinguir entre «*software* libre» y «*freeware*». El término «*freeware*» significa que el *software* se puede redistribuir libremente, pero por lo general no ofrece la libertad para estudiar y modificar el código fuente, así que la mayoría de esos programas no son *software* libre. Véase el artículo «[Palabras y frases confusas que vale la pena evitar](http://www.gnu.org/philosophy/words-to-avoid.es.html)» para más detalles.

En la actualidad su nombre ha sido cambiado por Open Society Foundations. [N. del E.].

Vicent Larivière, Stefanie Haustein y Philippe Mongeon, «[The Oligopoly of Academic Publishers in the Digital Era](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0127502)». PLoS ONE 10, núm. 6. y «[The Obscene Profits of Commercial Scholarly Publishers](http://svpow.com/2012/01/13/the-obscene-profits-of-commercial-scholarly-publishers/)».

Ian Sample, «[Harvard University Says It Can’t Afford Journal Publishers’ Prices»](http://www.theguardian.com/science/2012/apr/24/harvard-university-journal-publishers-prices). *The Guardian*.

«[Academic Paywalls Mean Publish and Perish](http://www.aljazeera.com/indepth/opinion/2012/10/20121017558785551.html)», *Al Jazeera English*.

«[Sci-Hub Tears Down Academia’s ‘Illegal’ Copyright Paywalls](https://torrentfreak.com/sci-hub-tears-down-academias-illegal-copyright-paywalls-150627/)», TorrentFreak.

«[Save Ashgate Publishing](https://www.change.org/p/save-ashgate-publishing)», Change.org.

«[The Cost of Knowledge](http://thecostofknowledge.com/)».

De hecho, con el +++tpp+++ y el +++ttip+++ siendo negociados rápidamente, ningún registrador de dominios, proveedor de +++isp+++, servidor u organización de derechos humanos será capaz de prevenir que las industrias de los derechos de autor y las cortes criminalicen y cierren sitios *web* con toda la prontitud posible.

«[Court Orders Shutdown of Libgen, Bookfi and Sci-Hub](https://torrentfreak.com/court-orders-shutdown-of-libgen-bookfi-and-sci-hub-151102/)», TorrentFreak.

Aaron Swartz y anónimos, «Manifiesto de la guerrilla por el acceso abierto». Disponible en el capítulo anterior.